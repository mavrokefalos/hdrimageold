extern "C" {
#include <TH.h>
#include <luaT.h>
}

#define torch_(NAME) TH_CONCAT_3(torch_, Real, NAME)
#define torch_Tensor TH_CONCAT_STRING_3(torch., Real, Tensor)
#define libhdr_(NAME) TH_CONCAT_3(libhdr_, Real, NAME)

#include "generic/hdr.cpp"
#include "THGenerateAllTypes.h"

extern "C" { DLL_EXPORT int luaopen_libhdr(lua_State *L)
{
  libhdr_FloatMain_init(L);
  libhdr_DoubleMain_init(L);

  lua_newtable(L);
  lua_pushvalue(L, -1);
  lua_setglobal(L, "libhdr");

  lua_newtable(L);
  luaT_setfuncs(L, libhdr_DoubleMain__, 0);
  lua_setfield(L, -2, "double");

  lua_newtable(L);
  luaT_setfuncs(L, libhdr_FloatMain__, 0);
  lua_setfield(L, -2, "float");

  return 1;
}
}
