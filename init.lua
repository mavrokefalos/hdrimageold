require 'torch'
require 'paths'
require 'dok'

local hdrimage = {}

local types = {
    float = true,
    double = true
}

local type2tensor = {
   float = torch.FloatTensor(),
   double = torch.DoubleTensor()
}

local template = function(type)
   if types[type] then
      return type2tensor[type]
   else
      return torch.Tensor()
   end
end

local function loadEXR(filename, tensortype)
   require 'libexr'
   return template(tensortype).libexr.load(filename):permute(3,1,2)
end

local function loadHDR(filename, tensortype)
   require 'libhdr'
   return template(tensortype).libhdr.load(filename):permute(3,1,2)
end


local filetypes = {
   exr = {loader = loadEXR},
   hdr = {loader = loadHDR}
}

local function check(condition, message, fname)
    if not condition then
        dok.error(message, 'hdrimage.' .. fname)
    end
end

local function load(filename, tensortype,forceExt)
    check(torch.type(filename) == 'string','filename must be a string.','load')
    check(paths.filep(filename), 'File ' .. filename .. ' does not exist.','load')
    local ext = paths.extname(filename)
    if not forceExt then 
        check(filetypes[ext], 'Extension not supported: ' .. ext .. ' in ' .. filename,'load')
    end
    if not tensortype or not types[tensortype] then tensortype = 'float' end
   return filetypes[forceExt or ext].loader(paths.concat(filename), tensortype)
end
rawset(hdrimage, 'load', load)

return hdrimage