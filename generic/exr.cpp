#ifndef TH_GENERIC_FILE
#define TH_GENERIC_FILE "generic/exr.cpp"
#else

#include <ImfRgbaFile.h>
#include <thread>
#include <omp.h>

static int libexr_(Main_load)(lua_State *L)
{
    const char *filename = luaL_checkstring(L, 1);
    int nThreads = std::thread::hardware_concurrency();
    Imf::setGlobalThreadCount(nThreads);
    Imf::RgbaInputFile in(filename,nThreads);
    Imath::Box2i win = in.dataWindow();

    Imath::V2i dim(win.max.x - win.min.x + 1, win.max.y - win.min.y + 1);

    Imf::Rgba *pixelBuffer = new Imf::Rgba[dim.x * dim.y];

    int dx = win.min.x;
    int dy = win.min.y;

    in.setFrameBuffer(pixelBuffer - dx - dy * dim.x, 1, dim.x);
    in.readPixels(win.min.y, win.max.y);

    int width  = dim.x;
    int height = dim.y;
    int channels = 3;

    THTensor *tensor = NULL;
    tensor = THTensor_(newWithSize3d)( height, width,channels);
    real *tdata = THTensor_(data)(tensor);

    int tot = width * height * channels;
    #pragma omp parallel for num_threads(nThreads)
    for(int i = 0; i < tot; i += channels) {
        int j = i / 3;
        tdata[i    ] = (real) pixelBuffer[j].r;
        tdata[i + 1] = (real) pixelBuffer[j].g;
        tdata[i + 2] = (real) pixelBuffer[j].b;
    }

    delete[] pixelBuffer;
    luaT_pushudata(L, tensor, torch_Tensor);
    return 1;
}


static const luaL_Reg libexr_(Main__)[] =
{
  {"load", libexr_(Main_load)},
  {NULL, NULL}
};

extern "C" { DLL_EXPORT int libexr_(Main_init)(lua_State *L)
{
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, libexr_(Main__), "libexr");
  return 1;
}
}

#endif

