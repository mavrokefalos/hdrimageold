#ifndef TH_GENERIC_FILE
#define TH_GENERIC_FILE "generic/hdr.cpp"
#else

#include <stdio.h>
#include <string>
#include <iostream>

void libhdr_(Main_RGBE2Float)(unsigned char *colRGBE, real *colFloat)
{

    if((*(colRGBE) == 0) && (*(colRGBE + 1) == 0) &&
       (*(colRGBE + 2) == 0)) { 
        *(colFloat) = 0;
        *(colFloat + 1) = 0;
        *(colFloat + 2) = 0;
        return;
    }

    int E;
    real f;

    E = *(colRGBE + 3) - 128 - 8;
    f = ldexpf(1.0f, E);

    *(colFloat) = ((real)(*(colRGBE)) + 0.5f) * f;
    *(colFloat + 1) = ((real)(*(colRGBE + 1)) + 0.5f) * f;
    *(colFloat + 2) = ((real)(*(colRGBE + 2)) + 0.5f) * f;
}



static int libhdr_(Main_load)(lua_State *L)
{
    const char *filename = luaL_checkstring(L, 1);
    FILE *file = fopen(filename, "rb");
    char tmp[512];

    size_t retval;
    retval = fscanf(file, "%s\n", tmp);
    while(true) {
        std::string line = "";
        while(true) {
            line += fgets(tmp, 512, file);
            if(line.find("\n") != std::string::npos) break;
        }
        if(line.compare("\n") == 0)  break;
    }

    int width,height;
    retval = fscanf(file, "-Y %d +X %d", &height, &width);
    retval = fgetc(file);

     THTensor *tensor = NULL;
    tensor = THTensor_(newWithSize3d)(height,width,3);
    real *tdata = THTensor_(data)(tensor);

    //File size
    long int s_cur = ftell(file);
    retval = fseek(file, 0 , SEEK_END);
    long int s_end = ftell(file);
    retval = fseek(file, s_cur, SEEK_SET);
    int total = s_end - s_cur;

    //Compressed?
    if(total == (width * height * 4)) { //uncompressed
        unsigned char colRGBE[4];
        int c = 0;

        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                retval = fread(colRGBE, 1, 4, file);
                libhdr_(Main_RGBE2Float)(colRGBE, &tdata[c]);
                c += 3;
            }
        }
    } else { //RLE compressed
        unsigned char *buffer = new unsigned char[total];
        retval = fread(buffer, sizeof(unsigned char)*total, 1, file);

        int line_width3 = width * 3;
        int line_width4 = width * 4;

        unsigned char *buffer_line_start;
        unsigned char *buffer_line = new unsigned char[line_width4];
        int c = 4;
        int c_buffer_line = 0;
        //for each line
        for(int i = 0; i < height; i++) {
            buffer_line_start = &buffer[c - 4];

            int width_check  = buffer_line_start[2];
            int width_check2 = buffer_line_start[3];

            bool b1 = buffer_line_start[0] != 2;
            bool b2 = buffer_line_start[1] != 2;
            bool b3 = width_check  != (width >> 8); 
            bool b4 = width_check2 != (width & 0xFF);

            if(b1 || b2 || b3 || b4) {
                printf("ReadHDR ERROR: the file is not a RLE encoded .hdr file.\n");
                fclose(file);
            }

            for(int j = 0; j < 4; j++) {
                int k = 0;

                //decompression of a single channel line
                while(k < width) {
                    int num = buffer[c];

                    if(num > 128) {
                        num -= 128;

                        for(int l = k; l < (k + num); l++) {
                            buffer_line[l * 4 + j] = buffer[c + 1];
                        }

                        c += 2;
                        k += num;
                    } else {
                        for(int l = 0; l < num; l++) {
                            buffer_line[(l + k) * 4 + j] = buffer[c + 1 + l];
                        }

                        c += num + 1;
                        k += num;
                    }
                }
            }

            //From RGBE to Float
            for(int j = 0; j < width; j++) {
                libhdr_(Main_RGBE2Float)(&buffer_line[j * 4], &tdata[c_buffer_line + j * 3]);
            }
            c += 4;
            c_buffer_line += line_width3;
        }

        delete[] buffer_line;
        delete[] buffer;
    }

    fclose(file);

    luaT_pushudata(L, tensor, torch_Tensor);
    return 1;
}


static const luaL_Reg libhdr_(Main__)[] =
{
  {"load", libhdr_(Main_load)},
  {NULL, NULL}
};

extern "C" { DLL_EXPORT int libhdr_(Main_init)(lua_State *L)
{
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, libhdr_(Main__), "libhdr");
  return 1;
}
}

#endif

