extern "C" {
#include <TH.h>
#include <luaT.h>
}

#define torch_(NAME) TH_CONCAT_3(torch_, Real, NAME)
#define torch_Tensor TH_CONCAT_STRING_3(torch., Real, Tensor)
#define libexr_(NAME) TH_CONCAT_3(libexr_, Real, NAME)

#include "generic/exr.cpp"
#include "THGenerateAllTypes.h"

extern "C" { DLL_EXPORT int luaopen_libexr(lua_State *L)
{
  libexr_FloatMain_init(L);
  libexr_DoubleMain_init(L);

  lua_newtable(L);
  lua_pushvalue(L, -1);
  lua_setglobal(L, "libexr");

  lua_newtable(L);
  luaT_setfuncs(L, libexr_DoubleMain__, 0);
  lua_setfield(L, -2, "double");

  lua_newtable(L);
  luaT_setfuncs(L, libexr_FloatMain__, 0);
  lua_setfield(L, -2, "float");

  return 1;
}
}
